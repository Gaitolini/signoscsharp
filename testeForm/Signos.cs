﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SignosClass
{
 
    
    public class Signo
    {
        public string SignoNome { get; set; }
        public DateTime DataSignoIni { get; set; }
        public DateTime DataSignoFin { get; set; }
        public string Cor { get; set; }

        public Signo()
        {

        }

    }

    public class Aquario : Signo
    {

        public Aquario()
        {
            SignoNome = "Aquário";
            DataSignoIni = new DateTime(DateTime.Now.Year, 01, 21, 0, 0, 0, 0);
            DataSignoFin = new DateTime(DateTime.Now.Year, 02, 18, 0, 0, 0, 0);
            Cor = "Amarelo";

        }

    }

    public class Peixes : Signo
    {

        public Peixes()
        {
            SignoNome = "Peixes";
            DataSignoIni = new DateTime(DateTime.Now.Year, 02, 19, 0, 0, 0, 0);
            DataSignoFin = new DateTime(DateTime.Now.Year, 03, 20, 0, 0, 0, 0);
            Cor = "Amarelo";

        }

    }

    public class Aries : Signo {

        public Aries()
        {
            SignoNome = "Áries";
            DataSignoIni = new DateTime(DateTime.Now.Year, 03, 21, 0, 0, 0, 0);
            DataSignoFin = new DateTime(DateTime.Now.Year, 04, 20, 0, 0, 0, 0);
            Cor = "Verde";

        }

    }


    public class Touro : Signo
    {

        public Touro()
        {
            SignoNome = "Áries";
            DataSignoIni = new DateTime(DateTime.Now.Year, 04, 21, 0, 0, 0, 0);
            DataSignoFin = new DateTime(DateTime.Now.Year, 05, 20, 0, 0, 0, 0);
            Cor = "Amarelo";

        }
    }

    public class Gemeos : Signo
    {

        public Gemeos()
        {
            SignoNome = "Gêmeos";
            DataSignoIni = new DateTime(DateTime.Now.Year, 05, 21, 0, 0, 0, 0);
            DataSignoFin = new DateTime(DateTime.Now.Year, 06, 20, 0, 0, 0, 0);
            Cor = "Roxo";

        }
    }

    public class Cancer : Signo
    {

        public Cancer()
        {
            SignoNome = "Câncer";
            DataSignoIni = new DateTime(DateTime.Now.Year, 06, 21, 0, 0, 0, 0);
            DataSignoFin = new DateTime(DateTime.Now.Year, 07, 22, 0, 0, 0, 0);
            Cor = "Vermelho";

        }
    }

    public class Leao : Signo
    {

        public Leao()
        {
            SignoNome = "Leão";
            DataSignoIni = new DateTime(DateTime.Now.Year, 07, 23, 0, 0, 0, 0);
            DataSignoFin = new DateTime(DateTime.Now.Year, 08, 22, 0, 0, 0, 0);
            Cor = "Vermelho";

        }

    }

    public class Virgem : Signo
    {

        public Virgem()
        {
            SignoNome = "Virgem";
            DataSignoIni = new DateTime(DateTime.Now.Year, 08, 23, 0, 0, 0, 0);
            DataSignoFin = new DateTime(DateTime.Now.Year, 09, 22, 0, 0, 0, 0);
            Cor = "Vermelho";

        }

    }


    public class Libra : Signo
    {

        public Libra()
        {
            SignoNome = "Libra";
            DataSignoIni = new DateTime(DateTime.Now.Year, 09, 23, 0, 0, 0, 0);
            DataSignoFin = new DateTime(DateTime.Now.Year, 10, 22, 0, 0, 0, 0);
            Cor = "Vermelho";

        }

    }

    public class Escorpiao : Signo
    {

        public Escorpiao()
        {
            SignoNome = "Escorpião";
            DataSignoIni = new DateTime(DateTime.Now.Year, 10, 23, 0, 0, 0, 0);
            DataSignoFin = new DateTime(DateTime.Now.Year, 11, 21, 0, 0, 0, 0);
            Cor = "Vermelho";

        }

    }

    public class Sagitario : Signo
    {

        public Sagitario()
        {
            SignoNome = "Sagitário";
            DataSignoIni = new DateTime(DateTime.Now.Year, 11, 22, 0, 0, 0, 0);
            DataSignoFin = new DateTime(DateTime.Now.Year, 12, 21, 0, 0, 0, 0);
            Cor = "Preto";
        }

    }

    public class Capricornio : Signo
    {

        public Capricornio()
        {
            SignoNome = "Capricórnio";
            DataSignoIni = new DateTime(DateTime.Now.Year, 12, 22, 0, 0, 0, 0);
            DataSignoFin = new DateTime(DateTime.Now.Year, 01, 20, 0, 0, 0, 0);
            Cor = "Amarelo";

        }

    }



    public class Signos
    {
        protected Aries aries = new Aries() ;
        protected Touro touro = new Touro();
        protected Gemeos gemeos = new Gemeos();
        protected Cancer cancer = new Cancer();
        protected Leao leao = new Leao();
        protected Virgem virgem = new Virgem();
        protected Libra libra = new Libra();
        protected Escorpiao escorpiao = new Escorpiao();
        protected Sagitario sagitario = new Sagitario();
        protected Capricornio capricornio = new Capricornio();
        protected Aquario aquario = new Aquario();
        protected Peixes peixes = new Peixes();

        public Signo[] signosArr = new Signo[11];
            
        public Signos()
        {
            signosArr[0] = (Signo)aries;
            signosArr[1] = (Signo)touro;
            signosArr[2] = (Signo)gemeos;
            signosArr[3] = (Signo)cancer;
            signosArr[4] = (Signo)leao;
            signosArr[5] = (Signo)virgem;
            signosArr[6] = (Signo)libra;
            signosArr[7] = (Signo)escorpiao;
            signosArr[8] = (Signo)sagitario;
            signosArr[9] = (Signo)aquario;
            signosArr[10] = (Signo)peixes;
        }

        public Signo Find(DateTime dateToCheck)
        {
                      
            try
            {
                string temp = dateToCheck.ToString("dd/MM/yyyy");
                dateToCheck= Convert.ToDateTime(temp);             
            }
            catch
            {
                MessageBox.Show("Sorry The date not valid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign);
                return null;
            }

            foreach(Signo _signo in signosArr)
            {
                string datestrIni = Convert.ToString( _signo.DataSignoIni.Day) +'/'+ Convert.ToString(_signo.DataSignoIni.Month) +'/'+ Convert.ToString(dateToCheck.Year);
                DateTime dtIni = Convert.ToDateTime(datestrIni);

                string datestrFim = Convert.ToString(_signo.DataSignoFin.Day) + '/' + Convert.ToString(_signo.DataSignoFin.Month) + '/' + Convert.ToString(dateToCheck.Year);
                DateTime dtFin = Convert.ToDateTime(datestrFim);
                if (dateToCheck >=  dtIni && dateToCheck <= dtFin)
                {
                    return _signo;
                    
                }              
            }
            return null;
        }
    }
}


