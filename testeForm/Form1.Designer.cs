﻿namespace testeForm
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("Signos");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.calendario = new System.Windows.Forms.MonthCalendar();
            this.tvSignos = new System.Windows.Forms.TreeView();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btnOpenIni = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(722, 193);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(351, 183);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "label1";
            // 
            // calendario
            // 
            this.calendario.Location = new System.Drawing.Point(354, 12);
            this.calendario.Name = "calendario";
            this.calendario.ShowWeekNumbers = true;
            this.calendario.TabIndex = 2;
            this.calendario.DateSelected += new System.Windows.Forms.DateRangeEventHandler(this.calendario_DateSelected);
            // 
            // tvSignos
            // 
            this.tvSignos.Location = new System.Drawing.Point(627, 12);
            this.tvSignos.Name = "tvSignos";
            treeNode3.Name = "noSignos";
            treeNode3.Text = "Signos";
            this.tvSignos.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode3});
            this.tvSignos.Size = new System.Drawing.Size(170, 162);
            this.tvSignos.TabIndex = 3;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "INI";
            this.openFileDialog1.FileName = "ofdOpenINI";
            this.openFileDialog1.Filter = "|*.*INI";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 21);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(223, 20);
            this.textBox1.TabIndex = 4;
            // 
            // btnOpenIni
            // 
            this.btnOpenIni.Location = new System.Drawing.Point(250, 19);
            this.btnOpenIni.Name = "btnOpenIni";
            this.btnOpenIni.Size = new System.Drawing.Size(42, 23);
            this.btnOpenIni.TabIndex = 5;
            this.btnOpenIni.Text = "...";
            this.btnOpenIni.UseVisualStyleBackColor = true;
            this.btnOpenIni.Click += new System.EventHandler(this.btnOpenIni_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Astro-Aquarius-128.png");
            this.imageList1.Images.SetKeyName(1, "Astro-Pisces-128.png");
            this.imageList1.Images.SetKeyName(2, "Astro-Aries-128.png");
            this.imageList1.Images.SetKeyName(3, "Astro-Taurus-128.png");
            this.imageList1.Images.SetKeyName(4, "Astro-Gemini-128.png");
            this.imageList1.Images.SetKeyName(5, "Astro-Cancer-128.png");
            this.imageList1.Images.SetKeyName(6, "Astro-Leo-128.png");
            this.imageList1.Images.SetKeyName(7, "Astro-Virgo-128.png");
            this.imageList1.Images.SetKeyName(8, "Astro-Libra-128.png");
            this.imageList1.Images.SetKeyName(9, "Astro-Scorpio-128.png");
            this.imageList1.Images.SetKeyName(10, "Astro-Sagittarius-128.png");
            this.imageList1.Images.SetKeyName(11, "Astro-Capricorn-128.png");
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(354, 223);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(235, 174);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnOpenIni);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.tvSignos);
            this.Controls.Add(this.calendario);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MonthCalendar calendario;
        private System.Windows.Forms.TreeView tvSignos;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btnOpenIni;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

