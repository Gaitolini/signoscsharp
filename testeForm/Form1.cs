﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Humanizer;
using ACBr;
using GeoCode;
using SignosClass;

namespace testeForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            label1.Text = "";
            calendario.MaxSelectionCount = 1;
        }

        
        private void button1_Click(object sender, EventArgs e)
        {
            if (label1.Text == "")
            {
                label1.Text = "Hello world!";
            }else
            {
                label1.Text = "";

            }
            
        }

        private void calendario_DateSelected(object sender, DateRangeEventArgs e)
        {
            Signo signoAchado;
            Signos signos = new Signos();
            signoAchado = signos.Find(calendario.SelectionRange.Start.Date);

            if (signoAchado == null)
            {
                label1.Text = "Signo não foi achado!";
            } else
            {
                label1.Text = $"Signo: {signoAchado.SignoNome} Cor: {signoAchado.Cor}";
           
                pictureBox1.Image = imageList1.Images[signoAchado.DataSignoIni.Month - 1];
            }
              
            

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Signos signos = new Signos();
            tvSignos.Nodes.Clear();
            tvSignos.Nodes.Add("Signos");
            int i = 0;
            foreach (Signo _signo in signos.signosArr)
            {
                
                tvSignos.Nodes[0].Nodes.Add(_signo.SignoNome);
                tvSignos.Nodes[0].Nodes[i].Nodes.Add(_signo.DataSignoIni.ToString("dd/MM/yyyy"));
                i++;

            }
        }

        private void btnOpenIni_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            
        }
    }
}
